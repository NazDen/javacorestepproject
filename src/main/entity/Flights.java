package entity;

import Enum.CityArrival;
import dao.FlightsDAO;
import io.FileWorker;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Flights implements FlightsDAO {

    private List<Flight> flights = new ArrayList<>();
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public Flights(List<Flight> list) {
        this.flights = list;
    }
    //READ FROM FILE
    public Flights() throws IOException {
        String[] fl = FileWorker.read("flights.txt").split("\n");
        for (String s:fl) {
            Flight flight = new Flight(s);
            flights.add(flight);
        }
    }
    //FIND BY CITY TO FLY
    @Override
    public List<Flight> getFlightsByArrival(CityArrival cityArrival){
        return this.flights.stream()
                .filter(flight -> flight.getArrival() == cityArrival)
                .collect(Collectors.toList());
    }
    //FIND BY DATE
    @Override
    public List<Flight> getFlightsByDate(String date){
        List<Flight> listRoutsByDate = new ArrayList<>();
        flights.stream()
                .filter(flight -> dateFormat.format(flight.getDate()).equals(date))
                .forEach(listRoutsByDate::add);
        return listRoutsByDate;
    }
    //FIND BY FLIGHT NUMBER
    @Override
    public List<Flight> getFlightByFlightNumber(String flightNumber) {
        List<Flight> flights = new ArrayList<>();
        this.flights.stream()
                .filter(flight -> flight.getFlightRoute().equalsIgnoreCase(flightNumber))
                .forEach(flights::add);
        return flights;
    }
    //SEARCH FOR FLIGHTS IN NEXT 24 HOURS FROM CURRENT TIME
    @Override
    public List<Flight> getFlightsBy24Hours() {
        List<Flight> lisOfFlightBy24Hours = new ArrayList<>();
        long curentTime=new Date().getTime();
        long currentTimePlus24Hours = curentTime + 24 * 60 * 60 * 1000;
        flights.stream().filter(flight -> flight.getDate().getTime() <= currentTimePlus24Hours&& flight.getDate().getTime()>=curentTime)
                .sorted(Comparator.comparing(flight -> flight.getDate().getTime()))
                .forEach(lisOfFlightBy24Hours::add);
        return lisOfFlightBy24Hours;
    }

    @Override
    public String toString() {
        return flights.toString().replaceAll("^\\[|]$", "").replaceAll(",", "") ;
    }

    public List<Flight> getFlights() {
        return flights;
    }
}
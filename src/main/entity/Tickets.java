package entity;

import dao.TicketsDAO;
import io.FileWorker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Tickets implements TicketsDAO {

    private List<Ticket> tickets = new ArrayList<>();
    //TO SOUT IN CONSOLE
    private String printInfo = "---------------------------------------------------------------------------------------------------------\n" +
            "#\t" + " Full name:\t\t\t" + "TicketID:\t\t" + "Flight№:\t" + "From:\t\t" + "To:\t\t\t" + "Date:\t\t" + "Time:\t" + "Duration:\t\n" +
            "---------------------------------------------------------------------------------------------------------\n" +
            tickets.toString().replaceAll("^\\[|\\]$", "").replaceAll("\\,", "");

    //READ
    public Tickets() {
        File file = new File("tickets.txt");
        if (!file.exists() || file.length() == 0) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            String[] tk = new String[0];
            try {
                tk = FileWorker.read("tickets.txt").split("\n");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            for (String s : tk) {
                Ticket ticket = new Ticket(s);
                tickets.add(ticket);
            }
        }
    }

    public Tickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    //GET ALL
    @Override
    public List<Ticket> getAllTickets() {
        return tickets;
    }

    //SEARCH BY ID
    @Override
    public Ticket getTicketByID(int id) {
        return tickets.stream().filter(ticket -> ticket.getTicketId() == id).findFirst().get();
    }

    //FOR BOOKING
    @Override
    public void createTicket(Flight flight, String name, String surname) {
        User user = new User(name, surname);
        Ticket ticket1 = new Ticket(flight, user);
        if (!tickets.contains(ticket1)) {
            tickets.add(ticket1);
            flight.setFreeSeats(flight.getFreeSeats() - 1);

        } else {
            System.out.println("YOU CANT BOOK A LOT OF TICKETS ON ONE FLIGHT FOR ONE PASSENGER");
        }
    }

    //FOR CONSOLE 5 COMMAND
    @Override
    public void showUserTickets(String name, String surname) {
        System.out.println(printInfo);
        tickets.stream()
                .filter(ticket -> ticket.getUser().getSurname().equals(surname) && ticket.getUser().getName().equals(name))
                .forEach(ticket -> System.out.println(String.format("%-5d", getAllTickets().indexOf(ticket) + 1) + ticket));
        System.out.println("\n---------------------------------------------------------------------------------------------------------");
    }

    //FOR CONSOLE 4 COMAND
    @Override
    public void removeTicket(int id) {

//        getTicketByID(id).getFlight().setFreeSeats(getTicketByID(id).getFlight().getFreeSeats() + 1);
        tickets.remove(getTicketByID(id));

    }

    //DONT USE
    @Override
    public String toString() {
        return "";
    }

    //GET|SET
    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

}

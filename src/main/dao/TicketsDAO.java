package dao;

import entity.Flight;
import entity.Ticket;

import java.util.List;

public interface TicketsDAO {
    List<Ticket> getAllTickets();
    Ticket getTicketByID(int id);
    void createTicket(Flight flight, String name, String surname);
    void showUserTickets(String name, String surname);
    void removeTicket(int id);
}
